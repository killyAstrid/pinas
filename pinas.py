import sys
import os
import re
from functools import partial
from http.server import SimpleHTTPRequestHandler, HTTPServer, ThreadingHTTPServer
from http import HTTPStatus
import urllib.parse
import html
import io
import threading
import queue
import time
import zipfile
import logging

import pychromecast

listing_template = None
logger = logging.Logger(__name__)


class PostHTTPRequestHandler(SimpleHTTPRequestHandler):

    PLAYABLE_MIMES = ('mp4',)

    DIR_LINK = '<li class="list-group-item"><a href="{link}">{display_link}</a></li>'
    DIR_LINK_PLAY = '<li class="list-group-item"><div><a href="{link}">{display_link}</a></div><div><a href="{link}***">play on chomeo</a></div></li>'

    STORAGE_URL = 'http://192.168.2.1:8000{}'

    TEST_VID = 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4'

    def __init__(self, *args, **kwargs):
        self.q = queue.Queue()
        threading.Thread(target=self.work, daemon=True).start()
        super().__init__(*args, **kwargs)

    def work(self):
        while True:
            item = self.q.get()
            try:
                cast = pychromecast.get_chromecasts()[0]
                print(f'Working on {item}')
                cast.wait()
                controller = cast.media_controller
                controller.play_media(
                    self.STORAGE_URL.format(item), 'video/mp4')
                controller.block_until_active()
                controller.play()
                print(f"Sleeping for {controller.status.duration}")
                # while wait for loop over stuff
                # time.sleep(controller.status.duration)
                print(f'Finished {item}')
                self.q.task_done()
            except Exception as e:
                print(e)
                pass

    def do_POST(self):
        return self.save_file()

    def save_file(self):
        logger.info("File upload")
        remaining_bytes = int(self.headers['content-length'])
        content_type = self.headers['content-type']
        bound_header = content_type.split('=')[-1]
        bound, dispos, contentype = [
            self.rfile.readline() for _ in range(3)]
        remaining_bytes -= (2*len(bound) + len(dispos) + len(contentype)) + 2
        if not bound_header.replace('\r\n', '') in bound.decode():
            return self.send_error(HTTPStatus.BAD_REQUEST,
                                   message='Wrongly formatted')
        try:
            filename = re.findall(r'filename="(.*)"', dispos.decode())[0]
        except IndexError:
            return self.send_error(HTTPStatus.BAD_REQUEST,
                                   message='Wrongly formatted')
        path = self.translate_path(self.path)
        filepath = os.path.join(path, filename)
        try:
            # strip the newline thing
            line = self.rfile.readline()
            remaining_bytes -= len(line)
            with open(filepath, 'wb') as f:
                while remaining_bytes > 0:
                    line = self.rfile.readline()
                    remaining_bytes -= len(line)
                    if remaining_bytes == 0:
                        line = line[:-1]
                        if line.endswith(b'\r'):
                            line = line[:-1]
                    f.write(line)
            if filename.endswith('.zip'):
                logger.info("Encountered ZIPPIE")
                logger.info("Extracting..")
                with zipfile.ZipFile(filepath, 'r') as zip_ref:
                    zip_ref.extractall(path)
                    logger.info("Done extracting.")
                os.remove(filepath)
                logger.info("Removed original")
            return self.do_GET()
        except IOError:
            return self.send_error(HTTPStatus.BAD_REQUEST,
                                   message=f'Cannot write to file {filepath}')

    def do_GET(self):
        if self.path.endswith('***'):
            self.q.put(self.path.replace('***', ''))
            self.path = "/"+"/".join(self.path.split('/')[:-1]) + "/"
        return super().do_GET()

    def list_directory(self, path):
        if not listing_template:
            return super().list_directory(path)
        try:
            list = os.listdir(path)
        except OSError:
            self.send_error(
                HTTPStatus.NOT_FOUND,
                "No permission to list directory")
            return None
        list.sort(key=lambda a: a.lower())
        try:
            displaypath = urllib.parse.unquote(self.path,
                                               errors='surrogatepass')
        except UnicodeDecodeError:
            displaypath = urllib.parse.unquote(path)
        displaypath = html.escape(displaypath, quote=False)
        enc = sys.getfilesystemencoding()
        dir_list = []
        for name in list:
            fullname = os.path.join(path, name)
            displayname = linkname = name
            # Append / for directories or @ for symbolic links
            if os.path.isdir(fullname):
                displayname = name + "/"
                linkname = name + "/"
            if os.path.islink(fullname):
                displayname = name + "@"
            link = (urllib.parse.quote(linkname,
                                       errors='surrogatepass'),
                    html.escape(displayname, quote=False))
            if name.split(".")[-1] in self.PLAYABLE_MIMES:
                dir_list_entry = self.DIR_LINK_PLAY.format_map(
                    {"link": link[0], "display_link": link[1]})
            else:
                dir_list_entry = self.DIR_LINK.format_map(
                    {"link": link[0], "display_link": link[1]})
                # Note: a link to a directory displays with @ and links with /
            dir_list.append(dir_list_entry)

        total = listing_template.replace('{{CURRENT_DIR}}', path.replace(str(os.getcwd()), '')).replace(
            '{{LISTINGS}}', '\n'.join(dir_list))
        encoded = total.encode(enc, 'surrogateescape')
        f = io.BytesIO()
        f.write(encoded)
        f.seek(0)
        self.send_response(HTTPStatus.OK)
        self.send_header("Content-type", "text/html; charset=%s" % enc)
        self.send_header("Content-Length", str(len(encoded)))
        self.end_headers()
        return f


def run(HandlerClass,
        protocol="HTTP/1.0", port=8000, bind=""):
    """Test the HTTP request handler class.

    This runs an HTTP server on port 8000 (or the port argument).

    """
    server_address = (bind, port)

    HandlerClass.protocol_version = protocol
    with ThreadingHTTPServer(server_address, HandlerClass) as httpd:
        host, port = httpd.socket.getsockname()
        serve_message = f"Serving HTTP on {host} port {port} (http://{host}:{port}/) ..."
        print(serve_message)
        try:
            httpd.serve_forever()
        except KeyboardInterrupt:
            print("\nKeyboard interrupt received, exiting.")
            sys.exit(0)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--bind', '-b', default='', metavar='ADDRESS',
                        help='Specify alternate bind address '
                             '[default: all interfaces]')
    parser.add_argument('port', action='store',
                        default=8000, type=int,
                        nargs='?',
                        help='Specify alternate port [default: 8000]')
    parser.add_argument('--directory', '-d', default='files',
                        help='Specify relative alternative directory '
                        '[default: .files]')
    args = parser.parse_args()

    dir = os.path.dirname(os.path.realpath(__file__))
    try:
        with open(os.path.join(dir, 'listing.html'), 'r') as f:
            listing_template = f.read()
    except:
        pass
    file_dir = os.path.join(dir, args.directory)
    handler_class = partial(PostHTTPRequestHandler,
                            directory=file_dir)
    run(HandlerClass=handler_class, port=args.port, bind=args.bind)
